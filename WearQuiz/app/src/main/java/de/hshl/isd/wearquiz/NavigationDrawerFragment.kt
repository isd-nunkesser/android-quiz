package de.hshl.isd.wearquiz

interface NavigationDrawerFragment {
    val navDrawerText: CharSequence
    val navDrawerDrawable: Int

}